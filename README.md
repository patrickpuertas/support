# Central de suporte da Singu

Ao registrar um bug datalhe bem o problema e coloque todas as informações que tiver em mãos como a plataforma, id's, qual a ação que foi executada, etc. Adicione evidências como prints, fotos ou links para o problema. É importante realçar os passos que foram executados: 

![Bug reporting](http://3.bp.blogspot.com/-i4P-QfIoUSE/T0dx50bJ53I/AAAAAAAAAcw/DzgWNO_KgBY/s1600/117+art+of+bug+reporting.png)

#### Modelo:

    Titlulo: Deve resumir o problema sem expor os detalhes
    Descrição: Detalhamento do problema seguindo o modelo abaixo:
    * Passo a passo para reproduzir
    * Comportamento atual
    * Comportamento esperado
    * Explique o grau de importancia do bug


&nbsp;
**Ex:** `Entrei` na plataforma, `cliquei` na aba de agendamentos, `pesquisei` Tallis Gomes no campo de input, `pressionei` o botão de busca. Esperava encontrar um agendamento de `Tallis Gomes` mas encontrei um agendamento de `João Fernandes`.  O bug aconteceu apenas em alguns casos e não é de completa importancia pois ainda conseguimos achar o agendamento do cliente pela pagina dos usuários, mas facilitaria nossa vida se tivessemos esse bug corrigido.

&nbsp;
## Como Reportar
Você pode adicionar uma issue de 2 formas:

    1 - Enviando um email para: incoming+SinguDevCrew/support@gitlab.com
    2 - Diretamente no painel de issues https://gitlab.com/SinguDevCrew/support/issues